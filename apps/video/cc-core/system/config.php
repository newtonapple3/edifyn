<?php

// Database Credentials
define ('DB_HOST', 'localhost');
define ('DB_PORT', '3306');
define ('DB_USER', 'root');
define ('DB_PASS', 'root');
define ('DB_NAME', 'edifyn');
define ('DB_PREFIX', 'video');

// FTP Credentials
define ('FTP_HOST', '');
define ('FTP_USER', '');
define ('FTP_PASS', '');
define ('FTP_PATH', '');
define ('FTP_SSL', false);